#!/bin/bash

docker login registry.gitlab.com
docker build -t registry.gitlab.com/andms/postgres-profile-aggregate .
docker push registry.gitlab.com/andms/postgres-profile-aggregate

docker-compose up -d

sleep 5

docker cp profiles.csv postgres-profile-aggregate:/tmp/profiles.csv

docker exec -it postgres-profile-aggregate psql -U 'postgres' -d 'postgres' -c "\COPY profile(name,username,email) FROM '/tmp/profiles.csv' DELIMITER ',' CSV HEADER;"

./create-connectors.sh